provider "null" {}

resource "null_resource" "master" {
  connection {
    type        = "ssh"
    host        = 
    user        = "root"
    password = 
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "curl -LO https://dl.k8s.io/release/`curl -LS https://dl.k8s.io/release/stable.txt`/bin/linux/amd64/kubectl",
      "chmod +x ./kubectl",
      "sudo mv ./kubectl /usr/local/bin/kubectl",
      "curl -sfL https://get.k3s.io | sh -",
      "sudo cat /var/lib/rancher/k3s/server/node-token > ~/config.txt",
      "yum install sshpass",
      "sshpass -p '' ssh root@ 'exit'",
      "sshpass -p '*' scp ~/config.txt root@:~/",
    ]
  }
}

resource "null_resource" "worker" {
  connection {
    type        = "ssh"
    host        = 
    user        = "root"
    password = 
  }

  provisioner "remote-exec" {
    inline = [
     "sudo yum update -y",
      "curl -LO https://dl.k8s.io/release/`curl -LS https://dl.k8s.io/release/stable.txt`/bin/linux/amd64/kubectl",
      "chmod +x ./kubectl",
      "sudo mv ./kubectl /usr/local/bin/kubectl",
      "export TOKEN=$(cat ~/config.txt)",
      "export URL=$(cat ~/configip.txt)",
      "curl -sfL https://get.k3s.io | K3S_URL=https://146.190.155.246:6443 K3S_TOKEN=$TOKEN sh -",
    ]
  }
  depends_on = [null_resource.master]
}